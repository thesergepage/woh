#include "code/woden-heart.h"

struct s_woden f_add_ciphed_woden_command(struct s_woden _woden, struct s_woden_ciphed_command _command) {
    t_unsigned8 v_index = 0;
    for (; v_index < _command.v_quantity; v_index++) {
        _woden.v_command_sequence[_woden.v_command_sequence_size++] = _command.v_commands[v_index];
    }

    return _woden;
}
