#include "code/woden-heart.h"

struct s_bits4 f_ciphe_bits4(t_unsigned8 _value) {
    struct s_bits4 v_result;

    v_result.v_bit0 = f_get_bit_from_unsigned8(_value, 0);
    v_result.v_bit1 = f_get_bit_from_unsigned8(_value, 1);
    v_result.v_bit2 = f_get_bit_from_unsigned8(_value, 2);
    v_result.v_bit3 = f_get_bit_from_unsigned8(_value, 3);

    return v_result;
}
