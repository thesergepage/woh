#include "code/woden-heart.h"

struct s_bits8 f_ciphe_bits8(t_unsigned8 _value) {
    struct s_bits8 v_result;

    t_unsigned8 v_index = 0, v_offset = 0;
    for (; v_index < 2; v_index++) {
        v_result.v_bits[v_index].v_bit0 = f_get_bit_from_unsigned8(_value, v_offset++);
        v_result.v_bits[v_index].v_bit1 = f_get_bit_from_unsigned8(_value, v_offset++);
        v_result.v_bits[v_index].v_bit2 = f_get_bit_from_unsigned8(_value, v_offset++);
        v_result.v_bits[v_index].v_bit3 = f_get_bit_from_unsigned8(_value, v_offset++);
    }

    return v_result;
}
