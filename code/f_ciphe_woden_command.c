#include "code/woden-heart.h"

struct s_woden_ciphed_command f_ciphe_woden_command(enum e_woden_commands _identifier, t_unsigned8 _size,
        t_unsigned8 _content[__common_woden_command_sequence_size] )
        {
            const t_unsigned8 v_identifier = (t_unsigned8)_identifier;

            t_unsigned8 v_content_index = 0;

            struct s_woden_ciphed_command v_result;

            v_result.v_quantity = 0;

            v_result.v_commands[v_result.v_quantity].v_bits[0] = f_ciphe_bits4(v_identifier);

            switch(_identifier)
            {
                case ec_begin:
                for(v_content_index = 0; v_content_index < _size; v_content_index++)
                {
                    v_result.v_commands[v_result.v_quantity].v_bits[1] = f_set_bits4_unsigned8(
                            v_result.v_commands[v_result.v_quantity].v_bits[1],
                            __common_collection1(t_bit, 4)
                            {
                                f_get_bit_from_unsigned8(_content[v_content_index], 0),
                                f_get_bit_from_unsigned8(_content[v_content_index], 1),
                                f_get_bit_from_unsigned8(_content[v_content_index], 2),
                                f_get_bit_from_unsigned8(_content[v_content_index], 3)
                            }
                    );

                    ++v_result.v_quantity;

                    v_result.v_commands[v_result.v_quantity].v_bits[0] = f_set_bits4_unsigned8(
                            v_result.v_commands[v_result.v_quantity].v_bits[0],
                            __common_collection1(t_bit, 4)
                            {
                                f_get_bit_from_unsigned8(_content[v_content_index], 4),
                                f_get_bit_from_unsigned8(_content[v_content_index], 5),
                                f_get_bit_from_unsigned8(_content[v_content_index], 6),
                                f_get_bit_from_unsigned8(_content[v_content_index], 7)
                            }
                    );
                }
                break;

                case ec_stop:
                ++v_result.v_quantity;
                break;

                case ec_addition:
                case ec_subtraction:
                case ec_multiplication:
                case ec_division:
                v_result.v_commands[v_result.v_quantity].v_bits[1] = f_set_bits4_unsigned8(
                        v_result.v_commands[v_result.v_quantity].v_bits[1],
                        __common_collection1(t_bit, 4)
                        {
                            f_get_bit_from_unsigned8(_content[v_content_index], 0),
                            f_get_bit_from_unsigned8(_content[v_content_index], 1),
                            f_get_bit_from_unsigned8(_content[v_content_index], 2),
                            f_get_bit_from_unsigned8(_content[v_content_index], 3)
                        }
                );

                ++v_result.v_quantity;
                ++v_content_index;

                v_result.v_commands[v_result.v_quantity].v_bits[0] = f_set_bits2_unsigned8_specified(
                        v_result.v_commands[v_result.v_quantity].v_bits[0],
                        _content[v_content_index],

                        __common_collection1(t_unsigned8, 2)
                        {
                            0, 1
                        },

                        __common_collection1(t_unsigned8, 2)
                        {
                            0, 1
                        }
                );

                ++v_content_index;

                v_result.v_commands[v_result.v_quantity].v_bits[0] = f_set_bits2_unsigned8_specified(
                        v_result.v_commands[v_result.v_quantity].v_bits[0],
                        _content[v_content_index],

                        __common_collection1(t_unsigned8, 2)
                        {
                            2, 3
                        },

                        __common_collection1(t_unsigned8, 2)
                        {
                            0, 1
                        }
                );

                v_result.v_commands[v_result.v_quantity].v_bits[1] = f_set_bits2_unsigned8_specified(
                        v_result.v_commands[v_result.v_quantity].v_bits[1],
                        _content[v_content_index],

                        __common_collection1(t_unsigned8, 2)
                        {
                            0, 1
                        },

                        __common_collection1(t_unsigned8, 2)
                        {
                            2, 3
                        }
                );

                ++v_content_index;

                v_result.v_commands[v_result.v_quantity].v_bits[1] = f_set_bits2_unsigned8_specified(
                        v_result.v_commands[v_result.v_quantity].v_bits[1],
                        _content[v_content_index],

                        __common_collection1(t_unsigned8, 2)
                        {
                            2, 3
                        },

                        __common_collection1(t_unsigned8, 2)
                        {
                            0, 1
                        }
                );

                ++v_result.v_quantity;
                break;

                case ec_assingment:
                v_result.v_commands[v_result.v_quantity].v_bits[1] = f_set_bits4_unsigned8(
                        v_result.v_commands[v_result.v_quantity].v_bits[1],
                        __common_collection1(t_bit, 4)
                        {
                            f_get_bit_from_unsigned8(_content[v_content_index], 0),
                            f_get_bit_from_unsigned8(_content[v_content_index], 1),
                            f_get_bit_from_unsigned8(_content[v_content_index], 2),
                            f_get_bit_from_unsigned8(_content[v_content_index], 3)
                        }
                );

                ++v_result.v_quantity;
                ++v_content_index;

                v_result.v_commands[v_result.v_quantity].v_bits[0] = f_set_bits2_unsigned8_specified(
                        v_result.v_commands[v_result.v_quantity].v_bits[0],
                        _content[v_content_index],

                        __common_collection1(t_unsigned8, 2)
                        {
                            0, 1
                        },

                        __common_collection1(t_unsigned8, 2)
                        {
                            0, 1
                        }
                );

                ++v_content_index;

                v_result.v_commands[v_result.v_quantity].v_bits[0] = f_set_bits2_unsigned8_specified(
                        v_result.v_commands[v_result.v_quantity].v_bits[0],
                        _content[v_content_index],

                        __common_collection1(t_unsigned8, 2)
                        {
                            2, 3
                        },

                        __common_collection1(t_unsigned8, 2)
                        {
                            0, 1
                        }
                );

                v_result.v_commands[v_result.v_quantity].v_bits[1] = f_set_bits4_unsigned8_specified(
                        v_result.v_commands[v_result.v_quantity].v_bits[1],
                        _content[v_content_index],

                        __common_collection1(t_unsigned8, 4)
                        {
                            0, 1, 2, 3
                        },

                        __common_collection1(t_unsigned8, 4)
                        {
                            2, 3, 4, 5
                        }
                );

                ++v_result.v_quantity;

                v_result.v_commands[v_result.v_quantity].v_bits[0] = f_set_bits2_unsigned8_specified(
                        v_result.v_commands[v_result.v_quantity].v_bits[0],
                        _content[v_content_index],

                        __common_collection1(t_unsigned8, 2)
                        {
                            0, 1
                        },

                        __common_collection1(t_unsigned8, 2)
                        {
                            6, 7
                        }
                );

                ++v_result.v_quantity;
                break;
            }

            return v_result;
        }
