#include "code/woden-heart.h"

t_unsigned8 f_deciphe_bits4(struct s_bits4 _bits) {
    t_unsigned8 v_result = 0;
    
    v_result = f_set_bit_to_unsigned8(v_result, _bits.v_bit0, 0);
    v_result = f_set_bit_to_unsigned8(v_result, _bits.v_bit1, 1);
    v_result = f_set_bit_to_unsigned8(v_result, _bits.v_bit2, 2);
    v_result = f_set_bit_to_unsigned8(v_result, _bits.v_bit3, 3);
    
    return v_result;
}
