#include "code/woden-heart.h"

t_unsigned8 f_deciphe_bits8(struct s_bits8 _bits) {
    t_unsigned8 v_result = 0;
    
    t_unsigned8 v_index = 0, v_offset = 0;
    for (; v_index < __common_woden_command_bits_size; v_index++) {
        v_result = f_set_bit_to_unsigned8(v_result, _bits.v_bits[v_index].v_bit0, v_offset++);
        v_result = f_set_bit_to_unsigned8(v_result, _bits.v_bits[v_index].v_bit1, v_offset++);
        v_result = f_set_bit_to_unsigned8(v_result, _bits.v_bits[v_index].v_bit2, v_offset++);
        v_result = f_set_bit_to_unsigned8(v_result, _bits.v_bits[v_index].v_bit3, v_offset++);
    }
    
    return v_result;
}
