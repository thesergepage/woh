#include "code/woden-heart.h"

struct s_woden f_deinitialise_woden(struct s_woden* _woden) {
    t_unsigned8 v_index = 0, v_master_index = 0, v_command_index = 0;
    
    (*_woden).v_command_pointer = 0;
    (*_woden).v_command_sequence_size = 0;
    
    for (; v_master_index < 8; v_master_index++) {
        for (v_command_index = 0; v_command_index < 2; v_command_index++) {
            (*_woden).v_command_sequence[v_master_index].v_bits[v_command_index] = f_reset_bits4(
                    (*_woden).v_command_sequence[v_master_index].v_bits[v_command_index]);
        }
    }
    
    for (; v_index < 8; v_index++) {
        (*_woden).v_cells_unsigned8[v_index].v_frame = 0;
    }
    
    return (*_woden);
}
