#include "code/woden-heart.h"

t_unsigned8 f_get_bit_from_unsigned8(t_unsigned8 _value, t_unsigned8 _offset) {
    return (_value & (1 << _offset)) == 0 ? 0 : 1;
}
