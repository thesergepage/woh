#include "code/woden-heart.h"

struct s_woden f_handle_woden(struct s_woden _woden, enum e_woden_faults* _fault) {
    _woden.v_command_pointer = 0;
    
    while (_woden.v_command_pointer < _woden.v_command_sequence_size) {
        _woden = f_handle_woden_command(_woden, f_next_woden_command(&_woden), _fault);
    }
    
    return _woden;
}
