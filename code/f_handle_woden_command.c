#include "code/woden-heart.h"

struct s_woden f_handle_woden_command(struct s_woden _woden, struct s_woden_command _command, enum e_woden_faults* _fault) {
    struct s_woden v_woden = _woden;

    t_unsigned8 v_left_cell_index = 0, v_left_cell_bits = 0;
    t_unsigned8 v_right_cell_index = 0, v_right_cell_bits = 0;

    t_unsigned8 v_right_raw = 0;

    t_unsigned8 v_command_identifier = f_deciphe_bits4(_command.v_bits[0]);

    if (f_is_woden_command(v_command_identifier) == __common_no) {
        struct s_bits8 v_full_command;

        v_full_command.v_bits[0] = _command.v_bits[1];

        _command = f_next_woden_command(&v_woden);

        v_full_command.v_bits[1] = _command.v_bits[0];

        v_command_identifier = f_deciphe_bits8(v_full_command);

        if (f_is_woden_command(v_command_identifier) == __common_no) {
            *_fault = ef_unknown_identifier;

            return _woden;
        }
    }

    if (v_command_identifier == ec_begin) {
        struct s_woden_command v_full_command;

        v_full_command.v_bits[0] = _command.v_bits[1];

        _command = f_next_woden_command(&v_woden);

        v_full_command.v_bits[1] = _command.v_bits[0];

        if (f_deciphe_bits4(f_probe_woden_command(v_woden).v_bits[0]) != (t_unsigned8) ec_end) {
            *_fault = ef_wrong_sequence;

            return _woden;
        }

        f_next_woden_command(&v_woden);

        return f_handle_woden_command(v_woden, v_full_command, _fault);
    } else if (v_command_identifier == ec_stop) {
        v_woden.v_command_pointer = v_woden.v_command_sequence_size;
    } else if (v_command_identifier == ec_assingment) {
        v_left_cell_index = f_deciphe_bits4(_command.v_bits[1]);

        _command = f_next_woden_command(&v_woden);

        v_left_cell_bits = f_set_bit_to_unsigned8(v_left_cell_bits, _command.v_bits[0].v_bit0, 0);
        v_left_cell_bits = f_set_bit_to_unsigned8(v_left_cell_bits, _command.v_bits[0].v_bit1, 1);

        if (v_left_cell_index > __common_woden_command_sequence_size) {
            *_fault = ef_wrong_index;

            return _woden;
        }

        v_right_raw = f_set_bit_to_unsigned8(v_right_raw, _command.v_bits[0].v_bit2, 0);
        v_right_raw = f_set_bit_to_unsigned8(v_right_raw, _command.v_bits[0].v_bit3, 1);

        v_right_raw = f_set_bit_to_unsigned8(v_right_raw, _command.v_bits[1].v_bit0, 2);
        v_right_raw = f_set_bit_to_unsigned8(v_right_raw, _command.v_bits[1].v_bit1, 3);

        v_right_raw = f_set_bit_to_unsigned8(v_right_raw, _command.v_bits[1].v_bit2, 4);
        v_right_raw = f_set_bit_to_unsigned8(v_right_raw, _command.v_bits[1].v_bit3, 5);

        _command = f_next_woden_command(&v_woden);

        v_right_raw = f_set_bit_to_unsigned8(v_right_raw, _command.v_bits[0].v_bit0, 6);
        v_right_raw = f_set_bit_to_unsigned8(v_right_raw, _command.v_bits[0].v_bit1, 7);

        switch (f_get_real_cell_bits(v_left_cell_bits)) {
            case 8:
            v_woden.v_cells_unsigned8[v_left_cell_index].v_frame = v_right_raw;
            break;

            default:
            *_fault = ef_unknown_cell;

            return _woden;
        }
    } else {
        v_left_cell_index = f_deciphe_bits4(_command.v_bits[1]);

        _command = f_next_woden_command(&v_woden);

        v_left_cell_bits = f_set_bit_to_unsigned8(v_left_cell_bits, _command.v_bits[0].v_bit0, 0);
        v_left_cell_bits = f_set_bit_to_unsigned8(v_left_cell_bits, _command.v_bits[0].v_bit1, 1);

        if (v_left_cell_index > __common_woden_command_sequence_size) {
            *_fault = ef_wrong_index;

            return _woden;
        }

        v_right_cell_index = f_set_bit_to_unsigned8(v_right_cell_index, _command.v_bits[0].v_bit2, 0);
        v_right_cell_index = f_set_bit_to_unsigned8(v_right_cell_index, _command.v_bits[0].v_bit3, 1);

        v_right_cell_index = f_set_bit_to_unsigned8(v_right_cell_index, _command.v_bits[1].v_bit0, 2);
        v_right_cell_index = f_set_bit_to_unsigned8(v_right_cell_index, _command.v_bits[1].v_bit1, 3);

        v_right_cell_bits = f_set_bit_to_unsigned8(v_right_cell_bits, _command.v_bits[1].v_bit2, 0);
        v_right_cell_bits = f_set_bit_to_unsigned8(v_right_cell_bits, _command.v_bits[1].v_bit3, 1);

        if (v_right_cell_index > __common_woden_command_sequence_size) {
            *_fault = ef_wrong_index;

            return _woden;
        }

        if (v_left_cell_bits < v_right_cell_bits) {
            *_fault = ef_wrong_bits;

            return _woden;
        }

        switch (f_get_real_cell_bits(v_right_cell_bits)) {
            case 8:
            v_right_raw = v_woden.v_cells_unsigned8[v_right_cell_index].v_frame;
            break;

            default:
            *_fault = ef_unknown_cell;

            return _woden;
        }

        switch (f_get_real_cell_bits(v_left_cell_bits)) {
            case 8:
            switch (v_command_identifier) {
                case ec_addition:
                v_woden.v_cells_unsigned8[v_left_cell_index].v_frame += v_right_raw;
                break;

                case ec_subtraction:
                v_woden.v_cells_unsigned8[v_left_cell_index].v_frame -= v_right_raw;
                break;

                case ec_multiplication:
                v_woden.v_cells_unsigned8[v_left_cell_index].v_frame *= v_right_raw;
                break;

                case ec_division:
                v_woden.v_cells_unsigned8[v_left_cell_index].v_frame /= v_right_raw;
                break;
            }
            break;

            default:
            *_fault = ef_unknown_cell;

            return _woden;
        }
    }

    return v_woden;
}
