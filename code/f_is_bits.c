#include "code/woden-heart.h"

t_unsigned8 f_is_bits(t_unsigned8 _value) {
    return _value >= 0 && _value < _eb_maximum ? __common_yes : __common_no;
}
