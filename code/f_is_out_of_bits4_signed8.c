#include "code/woden-heart.h"

t_unsigned8 f_is_out_of_bits4_signed8(t_signed8 _value) {
    t_unsigned8 v_index = 4;
    for (; v_index < 8; v_index++) {
        if (f_get_bit_from_signed8(_value, v_index) == 1) {
            return __common_yes;
        }
    }
    
    return __common_no;
}
