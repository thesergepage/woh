#include "code/woden-heart.h"

struct s_woden_command f_probe_woden_command(struct s_woden _woden) {
    return _woden.v_command_sequence[_woden.v_command_pointer];
}
