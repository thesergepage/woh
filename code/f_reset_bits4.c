#include "code/woden-heart.h"

struct s_bits4 f_reset_bits4(struct s_bits4 _set) {
    _set.v_bit0 = 0;
    _set.v_bit1 = 0;
    _set.v_bit2 = 0;
    _set.v_bit3 = 0;
    
    return _set;
}
