#include "code/woden-heart.h"

struct s_bits8 f_reset_bits8(struct s_bits8 _set) {
    t_unsigned8 v_index = 0;
    for (; v_index < 2; v_index++) {
        _set.v_bits[v_index] = f_reset_bits4(_set.v_bits[v_index]);
    }
    
    return _set;
}
