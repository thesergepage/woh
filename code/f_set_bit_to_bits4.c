#include "code/woden-heart.h"

struct s_bits4 f_set_bit_to_bits4(struct s_bits4 _bits, t_unsigned8 _bit, t_unsigned8 _value) {
    switch (_bit) {
        case 0:
        _bits.v_bit0 = _value;
        break;
        
        case 1:
        _bits.v_bit1 = _value;
        break;
        
        case 2:
        _bits.v_bit2 = _value;
        break;
        
        case 3:
        _bits.v_bit3 = _value;
        break;
    }
    
    return _bits;
}
