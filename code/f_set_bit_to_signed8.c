#include "code/woden-heart.h"

t_signed8 f_set_bit_to_signed8(t_signed8 _value, t_bit _bit, t_unsigned8 _offset) {
    return _value | (_bit << _offset);
}
