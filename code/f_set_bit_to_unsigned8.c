#include "code/woden-heart.h"

t_unsigned8 f_set_bit_to_unsigned8(t_unsigned8 _value, t_bit _bit, t_unsigned8 _offset) {
    return _value | (_bit << _offset);
}
