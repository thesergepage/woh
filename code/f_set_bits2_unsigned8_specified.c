#include "code/woden-heart.h"

struct s_bits4 f_set_bits2_unsigned8_specified(struct s_bits4 _bits, t_unsigned8 _raw, t_unsigned8 _indexes[2], t_unsigned8 _offsets[2]) {
    t_unsigned8 v_pointer = 0;
    for (; v_pointer < 2; v_pointer++) {
        switch (_indexes[v_pointer]) {
            case 0:
            _bits.v_bit0 = f_get_bit_from_unsigned8(_raw, _offsets[v_pointer]);
            break;
            
            case 1:
            _bits.v_bit1 = f_get_bit_from_unsigned8(_raw, _offsets[v_pointer]);
            break;
            
            case 2:
            _bits.v_bit2 = f_get_bit_from_unsigned8(_raw, _offsets[v_pointer]);
            break;
            
            case 3:
            _bits.v_bit3 = f_get_bit_from_unsigned8(_raw, _offsets[v_pointer]);
            break;
        }
    }
    
    return _bits;
}
