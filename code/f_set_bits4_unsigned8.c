#include "code/woden-heart.h"

struct s_bits4 f_set_bits4_unsigned8(struct s_bits4 _storage, t_bit _bits[4]) {
    _storage.v_bit0 = _bits[0];
    _storage.v_bit1 = _bits[1];
    _storage.v_bit2 = _bits[2];
    _storage.v_bit3 = _bits[3];
    
    return _storage;
}
