#include "code/woden-heart.h"

struct s_bits8 f_set_bits8_unsigned8(struct s_bits8 _storage, t_bit _bits[8]) {
    t_unsigned8 v_index = 0, v_offset = 0;
    for (; v_index < 2; v_index++) {
        _storage.v_bits[v_index].v_bit0 = _bits[v_offset++];
        _storage.v_bits[v_index].v_bit1 = _bits[v_offset++];
        _storage.v_bits[v_index].v_bit2 = _bits[v_offset++];
        _storage.v_bits[v_index].v_bit3 = _bits[v_offset++];
    }
    
    return _storage;
}
