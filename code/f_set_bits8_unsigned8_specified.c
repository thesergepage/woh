#include "code/woden-heart.h"

struct s_bits8 f_set_bits8_unsigned8_specified(struct s_bits8 _bits, t_unsigned8 _raw, t_unsigned8 _indexes[8], t_unsigned8 _offsets[8]) {
    t_unsigned8 v_pointer = 0;
    for (; v_pointer < 8; v_pointer++) {
        switch (_indexes[v_pointer]) {
            case 0:
            _bits.v_bits[0].v_bit0 = f_get_bit_from_unsigned8(_raw, _offsets[v_pointer]);
            break;
            
            case 1:
            _bits.v_bits[0].v_bit1 = f_get_bit_from_unsigned8(_raw, _offsets[v_pointer]);
            break;
            
            case 2:
            _bits.v_bits[0].v_bit2 = f_get_bit_from_unsigned8(_raw, _offsets[v_pointer]);
            break;
            
            case 3:
            _bits.v_bits[0].v_bit3 = f_get_bit_from_unsigned8(_raw, _offsets[v_pointer]);
            break;
            
            case 4:
            _bits.v_bits[1].v_bit0 = f_get_bit_from_unsigned8(_raw, _offsets[v_pointer]);
            break;
            
            case 5:
            _bits.v_bits[1].v_bit1 = f_get_bit_from_unsigned8(_raw, _offsets[v_pointer]);
            break;
            
            case 6:
            _bits.v_bits[1].v_bit2 = f_get_bit_from_unsigned8(_raw, _offsets[v_pointer]);
            break;
            
            case 7:
            _bits.v_bits[1].v_bit3 = f_get_bit_from_unsigned8(_raw, _offsets[v_pointer]);
            break;
        }
    }
    
    return _bits;
}
