#ifndef __woden_heart_guardian
#define __woden_heart_guardian



#include <../common/code/macro.h>
#include <../common/code/type.h>
#include <../common/code/enumeration.h>
#include <../common/code/structure.h>



struct s_woden {
    t_unsigned8 v_command_pointer, v_command_sequence_size;
    struct s_woden_command v_command_sequence[__common_woden_command_sequence_size ];

    union u_cell8 v_cells_unsigned8[8];
};



t_unsigned8 f_is_bits( t_unsigned8);
t_unsigned8 f_is_woden_command( t_unsigned8);

t_unsigned8 f_get_real_cell_bits( t_unsigned8);

t_unsigned8 f_is_out_of_bits4_signed8( t_signed8);
t_unsigned8 f_is_out_of_bits4_unsigned8( t_unsigned8);



t_unsigned8 f_set_bit_to_unsigned8( t_unsigned8, t_bit, t_unsigned8);
t_unsigned8 f_get_bit_from_unsigned8( t_unsigned8, t_unsigned8);

t_signed8 f_set_bit_to_signed8( t_signed8, t_bit, t_unsigned8);
t_unsigned8 f_get_bit_from_signed8( t_signed8, t_unsigned8);



struct s_bits4 f_set_bits4_unsigned8(struct s_bits4, t_bit[4]);
struct s_bits8 f_set_bits8_unsigned8(struct s_bits8, t_bit[8]);

struct s_bits4 f_set_bits2_unsigned8_specified(struct s_bits4, t_unsigned8, t_unsigned8[2], t_unsigned8[2]);

struct s_bits4 f_set_bits4_unsigned8_specified(struct s_bits4, t_unsigned8, t_unsigned8[4], t_unsigned8[4]);

struct s_bits8 f_set_bits8_unsigned8_specified(struct s_bits8, t_unsigned8, t_unsigned8[8], t_unsigned8[8]);

struct s_bits4 f_ciphe_bits4( t_unsigned8);
struct s_bits8 f_ciphe_bits8( t_unsigned8);

t_unsigned8 f_deciphe_bits4(struct s_bits4);
t_unsigned8 f_deciphe_bits8(struct s_bits8);

struct s_bits4 f_reset_bits4(struct s_bits4);
struct s_bits8 f_reset_bits8(struct s_bits8);

struct s_bits4 f_set_bit_to_bits4(struct s_bits4, t_unsigned8, t_unsigned8);



struct s_woden f_initialise_woden();
struct s_woden f_deinitialise_woden(struct s_woden*);
struct s_woden f_handle_woden(struct s_woden, enum e_woden_faults*);

struct s_woden_ciphed_command f_ciphe_woden_command(enum e_woden_commands, t_unsigned8, t_unsigned8[__common_woden_command_sequence_size] );
struct s_woden f_add_ciphed_woden_command(struct s_woden, struct s_woden_ciphed_command);

struct s_woden_command f_next_woden_command(struct s_woden*);
struct s_woden_command f_probe_woden_command(struct s_woden);

struct s_woden f_handle_woden_command(struct s_woden, struct s_woden_command, enum e_woden_faults*);



#endif
