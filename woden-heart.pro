TEMPLATE = lib
TARGET = woden-heart
CONFIG = ordered warn_on exceptions_off static



# development-stage
QMAKE_CFLAGS = -pedantic -W -Wall -Wextra -Wpedantic -std=gnu89 -funsigned-bitfields -funsigned-char -fsso-struct=little-endian -pipe -g3 -fvar-tracking -gdescribe-dies -ggnu-pubnames -grecord-gcc-switches -gas-loc-support -gas-locview-support -gcolumn-info -gstatement-frontiers -gdwarf -std=gnu89



SOURCES += \
    code/f_add_ciphed_woden_command.c \
    code/f_ciphe_bits4.c \
    code/f_ciphe_bits8.c \
    code/f_ciphe_woden_command.c \
    code/f_deciphe_bits4.c \
    code/f_deciphe_bits8.c \
    code/f_deinitialise_woden.c \
    code/f_get_bit_from_signed8.c \
    code/f_get_bit_from_unsigned8.c \
    code/f_get_real_cell_bits.c \
    code/f_handle_woden.c \
    code/f_handle_woden_command.c \
    code/f_initialise_woden.c \
    code/f_is_bits.c \
    code/f_is_out_of_bits4_signed8.c \
    code/f_is_out_of_bits4_unsigned8.c \
    code/f_is_woden_command.c \
    code/f_next_woden_command.c \
    code/f_probe_woden_command.c \
    code/f_reset_bits4.c \
    code/f_reset_bits8.c \
    code/f_set_bit_to_bits4.c \
    code/f_set_bit_to_signed8.c \
    code/f_set_bit_to_unsigned8.c \
    code/f_set_bits2_unsigned8_specified.c \
    code/f_set_bits4_unsigned8.c \
    code/f_set_bits4_unsigned8_specified.c \
    code/f_set_bits8_unsigned8.c \
    code/f_set_bits8_unsigned8_specified.c

HEADERS += \
    code/woden-heart.h
